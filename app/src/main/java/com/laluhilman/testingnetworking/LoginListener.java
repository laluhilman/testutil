package com.laluhilman.testingnetworking;

import com.jojonomic.jojoutilitieslib.model.JJUUserModel;

/**
 * Created by laluhilman on 09/05/18.
 */

public interface LoginListener {

    void onrequestSucces(String message, JJUUserModel userModel);
    void onrequestFailed(String message);
}
