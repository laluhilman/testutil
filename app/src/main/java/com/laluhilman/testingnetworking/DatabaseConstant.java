package com.laluhilman.testingnetworking;

/**
 * Created by laluhilman on 3/6/17.
 */

public class DatabaseConstant {

    public static final String DATABASE_NAME = "user.db";
    public static final int DATABASE_VERSION = 1;

    public static final String TABLE_NAME_USER = "user";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_EMAIL = "email";
    public static final String[] ALL_COLUMN_USER = {
            COLUMN_ID,
            COLUMN_ADDRESS,
            COLUMN_NAME,
            COLUMN_EMAIL};


    public static final String CREATE_USER = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_USER + " (" +
            COLUMN_ID + " TEXT, " +
            COLUMN_NAME + " TEXT, " +
            COLUMN_ADDRESS + " TEXT, " +
            COLUMN_EMAIL + " TEXT, " +
            "PRIMARY KEY ( " + COLUMN_ID + "," + COLUMN_NAME + ")"
            + ")";


    public static final String DROP_DATABASE_ATTENDANCE = "DROP TABLE IF EXISTS " + TABLE_NAME_USER;

    public static final String DELETE_DATABASE_ATTENDANCE = "DELETE FROM " + TABLE_NAME_USER;
}
