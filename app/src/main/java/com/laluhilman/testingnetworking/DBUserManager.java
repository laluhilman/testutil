package com.laluhilman.testingnetworking;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by laluhilman on 14/05/18.
 */

public class DBUserManager {


    UserModelContentValues userModelContentValues;
    private static DBUserManager singleton;

    public static DBUserManager getSingleton(Context context){
        if(singleton==null)
            singleton = new DBUserManager(context);
        return singleton;
    }


    public DBUserManager(Context context){
        userModelContentValues = new UserModelContentValues(context);
    }


    public void insertUser(UserModel userModel){

        userModelContentValues.beginTransaction();
        userModelContentValues.createData(DatabaseConstant.TABLE_NAME_USER, userModel);
        userModelContentValues.setTransactionSuccessful();
        userModelContentValues.endTransaction();

    }

    public List<UserModel> getuserModels() {
        List<UserModel> listUser = new ArrayList<>();
        listUser = getuserModels("");
        return listUser;
    }

    private List<UserModel> getuserModels(String condition) {
        List<UserModel> listUser = new ArrayList<>();

        if (condition != null && !condition.isEmpty()) {
            listUser = userModelContentValues.getAllData(DatabaseConstant.TABLE_NAME_USER, DatabaseConstant.ALL_COLUMN_USER, condition, null);
        } else {
            listUser = userModelContentValues.getAllData(DatabaseConstant.TABLE_NAME_USER, DatabaseConstant.ALL_COLUMN_USER, null, null);
        }
        return listUser;
    }


}
