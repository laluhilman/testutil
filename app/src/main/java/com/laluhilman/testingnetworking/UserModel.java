package com.laluhilman.testingnetworking;

/**
 * Created by laluhilman on 08/05/18.
 */

public class UserModel {

    String id ;
    String name;
    String address;
    String email;


    public UserModel() {
        this.id = "";
        this.name = "";
        this.address = "";
        this.email = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
