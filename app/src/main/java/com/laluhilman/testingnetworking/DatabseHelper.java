package com.laluhilman.testingnetworking;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.jojonomic.jojoutilitieslib.manager.database.JJUDatabaseHelper;

/**
 * Created by laluhilman on 14/05/18.
 */

public class DatabseHelper extends JJUDatabaseHelper {

    private static DatabseHelper singleton;

    public static DatabseHelper getSingleton(Context context) {
        if (singleton == null) {
            singleton = new DatabseHelper(context);
        }
        return singleton;
    }


    public DatabseHelper(Context context) {
        super(context, DatabaseConstant.DATABASE_NAME, DatabaseConstant.DATABASE_VERSION);
    }

    public DatabseHelper(Context context, String name, int version) {
        super(context, name, version);
    }

    @Override
    protected String getCode() {
        return "";
    }

    @Override
    protected String getFolderName() {
        return "";
    }

    @Override
    protected void onCreateDB(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(DatabaseConstant.CREATE_USER);

    }
}
