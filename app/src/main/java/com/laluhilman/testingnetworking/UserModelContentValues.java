package com.laluhilman.testingnetworking;

import android.content.ContentValues;
import android.content.Context;

import com.jojonomic.jojoutilitieslib.manager.database.JJUBaseDatabase;

/**
 * Created by laluhilman on 14/05/18.
 */

public class UserModelContentValues extends JJUBaseDatabase<UserModel> {

    public UserModelContentValues(Context context) {
        super(DatabseHelper.getSingleton(context));
    }

    @Override
    public ContentValues values(UserModel userModel) {
        ContentValues contentValues  = new ContentValues();
        contentValues.put(DatabaseConstant.COLUMN_ID, userModel.getId());
        contentValues.put(DatabaseConstant.COLUMN_NAME, userModel.getName());
        contentValues.put(DatabaseConstant.COLUMN_ADDRESS, userModel.getAddress());
        contentValues.put(DatabaseConstant.COLUMN_EMAIL, userModel.getEmail());

        return contentValues;
    }

    @Override
    protected UserModel cursorData() {
        UserModel userModel = new UserModel();
        userModel.setId(sourceCursor.cursorString(DatabaseConstant.COLUMN_ID));
        userModel.setName(sourceCursor.cursorString(DatabaseConstant.COLUMN_NAME));
        userModel.setEmail(sourceCursor.cursorString(DatabaseConstant.COLUMN_EMAIL));
        userModel.setAddress(sourceCursor.cursorString(DatabaseConstant.COLUMN_ADDRESS));
        return userModel;
    }
}
