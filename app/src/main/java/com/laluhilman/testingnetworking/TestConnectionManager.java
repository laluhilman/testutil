package com.laluhilman.testingnetworking;

import com.jojonomic.jojoutilitieslib.manager.connection.JJUConnectionManager;
import com.jojonomic.jojoutilitieslib.manager.connection.listener.ConnectionManagerListener;
import com.jojonomic.jojoutilitieslib.manager.database.JJUUserDatabaseManager;
import com.jojonomic.jojoutilitieslib.model.JJUCompanyModel;
import com.jojonomic.jojoutilitieslib.model.JJUConnectionResultModel;
import com.jojonomic.jojoutilitieslib.model.JJULocationModel;
import com.jojonomic.jojoutilitieslib.model.JJUPropertyModel;
import com.jojonomic.jojoutilitieslib.model.JJUUserModel;
import com.jojonomic.jojoutilitieslib.utilities.JJUConstant;
import com.jojonomic.jojoutilitieslib.utilities.JJUConstantConnection;
import com.jojonomic.jojoutilitieslib.utilities.JJUJojoSharePreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by laluhilman on 08/05/18.
 */

public class TestConnectionManager extends JJUConnectionManager {
    private static TestConnectionManager singleton;

    public static TestConnectionManager getSingleton(){
        if(singleton == null)
            singleton = new TestConnectionManager();

        return  singleton;
    }


    public void getData(final RequestListener listener){

        requestGET("https://api.androidhive.info/contacts/", new ConnectionManagerListener() {

            List<UserModel> listUserModel = new ArrayList<>();

            @Override
            public JJUConnectionResultModel onHandleSuccessData(String s) {
                JJUConnectionResultModel resultModel = new JJUConnectionResultModel();
                try {


                    JSONObject jsonObject = new JSONObject(s);

                    JSONArray listContatArray = jsonObject.getJSONArray("contacts");


                    for(int i = 0 ; i < listContatArray.length(); i++){
                        listUserModel.add(jsonToUserMOdel(listContatArray.getJSONObject(i)));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                return resultModel;
            }

            @Override
            public JJUConnectionResultModel onHandleFailedData(String s) {
                JJUConnectionResultModel resultModel = new JJUConnectionResultModel();
                resultModel.setIsError(true);

                return resultModel;
            }

            @Override
            public void onUpdateUI(JJUConnectionResultModel jjuConnectionResultModel) {

                if(listener!=null) {
                    if (jjuConnectionResultModel.isError()){
                        listener.onRequestFailed("Error");
                    } else {
                        listener.onRequestSucces("Sukses", listUserModel);
                    }

                }

            }
        });
    }

    public void login(final RequestListener listener){

        requestGET("https://api.androidhive.info/contacts/", new ConnectionManagerListener() {

            List<UserModel> listUserModel = new ArrayList<>();

            @Override
            public JJUConnectionResultModel onHandleSuccessData(String s) {
                JJUConnectionResultModel resultModel = new JJUConnectionResultModel();
                try {

                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray listContatArray = jsonObject.getJSONArray("contacts");

                    for(int i = 0 ; i < listContatArray.length(); i++){
                        listUserModel.add(jsonToUserMOdel(listContatArray.getJSONObject(i)));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                return resultModel;
            }

            @Override
            public JJUConnectionResultModel onHandleFailedData(String s) {
                JJUConnectionResultModel resultModel = new JJUConnectionResultModel();
                resultModel.setIsError(true);

                return resultModel;
            }

            @Override
            public void onUpdateUI(JJUConnectionResultModel jjuConnectionResultModel) {
                if(listener!=null) {
                    if (jjuConnectionResultModel.isError()){
                        listener.onRequestFailed("Error");
                    } else {
                        listener.onRequestSucces("Sukses", listUserModel);
                    }

                }
            }
        });
    }

    public void register(final RequestListener listener){

        requestGET("https://api.androidhive.info/contacts/", new ConnectionManagerListener() {

            List<UserModel> listUserModel = new ArrayList<>();

            @Override
            public JJUConnectionResultModel onHandleSuccessData(String s) {
                JJUConnectionResultModel resultModel = new JJUConnectionResultModel();
                try {

                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray listContatArray = jsonObject.getJSONArray("contacts");

                    for(int i = 0 ; i < listContatArray.length(); i++){
                        listUserModel.add(jsonToUserMOdel(listContatArray.getJSONObject(i)));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                return resultModel;
            }

            @Override
            public JJUConnectionResultModel onHandleFailedData(String s) {
                JJUConnectionResultModel resultModel = new JJUConnectionResultModel();
                resultModel.setIsError(true);

                return resultModel;
            }

            @Override
            public void onUpdateUI(JJUConnectionResultModel jjuConnectionResultModel) {
                if(listener!=null) {
                    if (jjuConnectionResultModel.isError()){
                        listener.onRequestFailed("Error");
                    } else {
                        listener.onRequestSucces("Sukses", listUserModel);
                    }

                }
            }
        });
    }

    public void requestLogin(String email, String password, final LoginListener listener) {
        try {
            JSONObject object = new JSONObject();
            object.put(JJUConstant.JSON_KEY_EMAIL, email);
            object.put(JJUConstant.JSON_KEY_PASSWORD, password);

            requestPOST("http://qa-jojoapi.jojonomic.id/app/api/v2/pro/user/login", object.toString(), new ConnectionManagerListener() {
                JJUUserModel userModel;
                @Override
                public JJUConnectionResultModel onHandleSuccessData(String response) {

                    JJUConnectionResultModel result = new JJUConnectionResultModel();
                    try {
                         userModel = parseUserFromJson(response);
                        JJUJojoSharePreference.putDataString(JJUJojoSharePreference.KEY_TOKEN, userModel.getUserToken());
                        JJUJojoSharePreference.putDataLong(JJUJojoSharePreference.KEY_LAST_UPDATE, 0);
                        JJUUserDatabaseManager.getSingleton(null).saveUser(userModel);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        String message = "Login Failed, Try again later";
                        result.setIsError(true);
                        result.setMessage(message);
                    }
                    return result;
                }

                @Override
                public JJUConnectionResultModel onHandleFailedData(String response) {
                    String message = "Login Failed, Try again later";
                    JJUConnectionResultModel result = new JJUConnectionResultModel(message, true);
                    try {
                        JSONObject jsonResponse = new JSONObject(response);
                        message = jsonResponse.getString(JJUConstant.JSON_KEY_MESSAGE);
                        result.setMessage(message);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return result;
                }

                @Override
                public void onUpdateUI(JJUConnectionResultModel model) {
                    if (listener != null) {
                        if (model.isError()) {
                            listener.onrequestFailed(model.getMessage());
                        } else {
                            listener.onrequestSucces(model.getMessage(), userModel);
                        }
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
            if (listener != null) {
                listener.onrequestFailed("Invalid request");
            }
        }
    }

    private UserModel jsonToUserMOdel(JSONObject jsonObject){
        UserModel userModel = new UserModel();

        try {
            userModel.setId(jsonObject.getString("id"));
            userModel.setName(jsonObject.getString("name"));
            userModel.setEmail(jsonObject.getString("email"));
            userModel.setAddress(jsonObject.getString("address"));


        } catch (JSONException e) {
            e.printStackTrace();
        }


        return userModel;

    }

    protected JJUUserModel parseUserFromJson(String stringResponse) throws JSONException {
        JSONObject jsonResponse = new JSONObject(stringResponse);
        jsonResponse = jsonResponse.getJSONObject(JJUConstant.JSON_KEY_USER);

        long birthdayLong = 0l;

        if (jsonResponse.has(JJUConstant.JSON_KEY_DATE_OF_BIRTH)) {
            String birthday = jsonResponse.getString(JJUConstant.JSON_KEY_DATE_OF_BIRTH);
            if (birthday != null && !birthday.equals("")) {
                SimpleDateFormat dateFormat = new SimpleDateFormat(JJUConstant.DATE_FORMAT_SERVER_V2, new Locale("en_US"));
                try {
                    birthdayLong = dateFormat.parse(birthday).getTime();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        JJUUserModel userModel = new JJUUserModel();
        if (jsonResponse.has(JJUConstant.JSON_KEY_TOKEN)) {
            userModel.setUserToken(jsonResponse.getString(JJUConstant.JSON_KEY_TOKEN));
            JJUJojoSharePreference.putDataString(JJUJojoSharePreference.KEY_TOKEN, userModel.getUserToken());
        }

        userModel.setUserFirstName(jsonResponse.getString(JJUConstant.JSON_KEY_FIRST_NAME));
        userModel.setUserLastName(jsonResponse.getString(JJUConstant.JSON_KEY_LAST_NAME));
        userModel.setUserBirthday(birthdayLong);
        userModel.setUserPhotoUrl(jsonResponse.getString(JJUConstant.JSON_KEY_PHOTO_URL));

        if (jsonResponse.has(JJUConstant.JSON_KEY_GENDER)) {
            userModel.setUserGender(jsonResponse.getString(JJUConstant.JSON_KEY_GENDER));
        }

        if (jsonResponse.has(JJUConstant.JSON_KEY_EMAIL)) {
            userModel.setUserEmail(jsonResponse.getString(JJUConstant.JSON_KEY_EMAIL));
        }

        if (jsonResponse.has(JJUConstant.JSON_KEY_PHONE_NUMBER)) {
            userModel.setUserPhoneNumber(jsonResponse.getString(JJUConstant.JSON_KEY_PHONE_NUMBER));
        }

        JJUPropertyModel propertyModel;
        if (jsonResponse.has(JJUConstant.JSON_KEY_PROPERTIES)) {
            JSONObject jsonProperty = jsonResponse.getJSONObject(JJUConstant.JSON_KEY_PROPERTIES);
            propertyModel = parsePropertyFromJson(jsonProperty);
        } else {
            propertyModel = new JJUPropertyModel();
        }
        userModel.setProperty(propertyModel);

        JJUCompanyModel companyModel;
        if (jsonResponse.has(JJUConstant.JSON_KEY_COMPANY)) {
            JSONObject jsonCompany = jsonResponse.getJSONObject(JJUConstant.JSON_KEY_COMPANY);
            companyModel = parseCompanyFromJson(jsonCompany);
        } else {
            companyModel = new JJUCompanyModel();
        }
        userModel.setCompany(companyModel);

        JJULocationModel locationModel;
        if (jsonResponse.has(JJUConstant.JSON_KEY_LOCATION)) {
            JSONObject jsonLocation = jsonResponse.getJSONObject(JJUConstant.JSON_KEY_LOCATION);
            locationModel = parseLocationFromJson(jsonLocation);
        } else {
            locationModel = new JJULocationModel();
        }
        userModel.setLocation(locationModel);

        if (jsonResponse.has(JJUConstant.JSON_KEY_SIGNATURE)) {
            JSONObject object = jsonResponse.getJSONObject(JJUConstant.JSON_KEY_SIGNATURE);
            if (object.has(JJUConstant.JSON_KEY_SIGN_URL)) {
                JJUJojoSharePreference.putDataString(JJUConstant.JSON_KEY_SIGN_URL, object.getString(JJUConstant.JSON_KEY_SIGN_URL));
            }
            if (object.has(JJUConstant.JSON_KEY_KEY)) {
                JJUJojoSharePreference.putDataString(JJUConstant.JSON_KEY_KEY, object.getString(JJUConstant.JSON_KEY_KEY));
            }
        }

        return userModel;
    }


    protected JJUPropertyModel parsePropertyFromJson(JSONObject jsonProperty) throws JSONException {
        JJUPropertyModel propertyModel = new JJUPropertyModel();
        propertyModel.setPropertyCurrency(jsonProperty.getString(JJUConstant.JSON_KEY_CURRENCY));
        propertyModel.setPropertyLanguage(jsonProperty.getString(JJUConstant.JSON_KEY_LANGUAGE));
        propertyModel.setPropertyPowerUser(jsonProperty.getBoolean(JJUConstant.JSON_KEY_POWER_USER));

        if (jsonProperty.has(JJUConstant.JSON_KEY_REMINDER)) {
            propertyModel.setPropertyReminder(jsonProperty.getString(JJUConstant.JSON_KEY_REMINDER));
        }

        if (jsonProperty.has(JJUConstant.JSON_KEY_MONTH_SALARY_DATE)) {
            propertyModel.setPropertyMonthSalaryDate(jsonProperty.getLong(JJUConstant.JSON_KEY_MONTH_SALARY_DATE));
        }

        if (jsonProperty.has(JJUConstant.JSON_KEY_DEFAULT_RATE_BOND)) {
            propertyModel.setPropertyDefaultRateBond(jsonProperty.getDouble(JJUConstant.JSON_KEY_DEFAULT_RATE_BOND));
        }

        if (jsonProperty.has(JJUConstant.JSON_KEY_DEFAULT_RATE_STOCK)) {
            propertyModel.setPropertyDefaultRateStock(jsonProperty.getDouble(JJUConstant.JSON_KEY_DEFAULT_RATE_STOCK));
        }

        if (jsonProperty.has(JJUConstant.JSON_KEY_DEFAULT_RATE_DEPOSIT)) {
            propertyModel.setPropertyDefaultRateDeposit(jsonProperty.getDouble(JJUConstant.JSON_KEY_DEFAULT_RATE_DEPOSIT));
        }

        boolean isHaveSecretariesMode = false;
        if (jsonProperty.has(JJUConstant.JSON_KEY_IS_HAVE_SECRETARIES_MODE)) {
            isHaveSecretariesMode = jsonProperty.getBoolean(JJUConstant.JSON_KEY_IS_HAVE_SECRETARIES_MODE);
        }
        JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_HAVE_SECRETARIES_MODE, isHaveSecretariesMode);

        return propertyModel;
    }

    protected JJULocationModel parseLocationFromJson(JSONObject jsonLocation) throws JSONException {
        JJULocationModel locationModel = new JJULocationModel();

        if (jsonLocation.has(JJUConstant.JSON_KEY_LAT)) {
            locationModel.setLocationLatitude(jsonLocation.getDouble(JJUConstant.JSON_KEY_LAT));
        }

        if (jsonLocation.has(JJUConstant.JSON_KEY_LNG)) {
            locationModel.setLocationLongitude(jsonLocation.getDouble(JJUConstant.JSON_KEY_LNG));
        }

        return locationModel;
    }

    protected JJUCompanyModel parseCompanyFromJson(JSONObject jsonCompany) throws JSONException {
        JJUCompanyModel companyModel = new JJUCompanyModel();
        companyModel.setCompanyCurrency(jsonCompany.getString(JJUConstant.JSON_KEY_CURRENCY));
        companyModel.setCompanyId(jsonCompany.getLong(JJUConstant.JSON_KEY_COMPANY_ID));
        companyModel.setCompanyApprovalId(jsonCompany.getLong(JJUConstant.JSON_KEY_APPROVER_ID));
        companyModel.setCompanyName(jsonCompany.getString(JJUConstant.JSON_KEY_COMPANY_NAME));
        companyModel.setCompanyGroupId(jsonCompany.getLong(JJUConstant.JSON_KEY_GROUP_ID));
        companyModel.setCompanyGroupName(jsonCompany.getString(JJUConstant.JSON_KEY_GROUP_NAME));


        //Tambahan
        if (jsonCompany.has(JJUConstant.JSON_KEY_EMPLOYEE_ID)) {
            companyModel.setCompanyEmployeeId(jsonCompany.getString(JJUConstant.JSON_KEY_EMPLOYEE_ID));
        }

        long purchaserId = 0;
        if (jsonCompany.has(JJUConstant.JSON_KEY_PURCHASER_ID)) {
            purchaserId = jsonCompany.getLong(JJUConstant.JSON_KEY_PURCHASER_ID);
        }
        JJUJojoSharePreference.putDataLong(JJUConstant.JSON_KEY_PURCHASER_ID, purchaserId);

        if (jsonCompany.has(JJUConstant.JSON_KEY_BANK_ACCOUNT_NUMBER)) {
            companyModel.setCompanyBankAccountNumber(jsonCompany.getString(JJUConstant.JSON_KEY_BANK_ACCOUNT_NUMBER));
        }

        if (jsonCompany.has(JJUConstant.JSON_KEY_BANK_ACCOUNT_NAME)) {
            companyModel.setCompanyBankAccountName(jsonCompany.getString(JJUConstant.JSON_KEY_BANK_ACCOUNT_NAME));
        }

        if (jsonCompany.has(JJUConstant.JSON_KEY_BANK_ACCOUNT_BRANCH)) {
            companyModel.setCompanyBankAccountBranch(jsonCompany.getString(JJUConstant.JSON_KEY_BANK_ACCOUNT_BRANCH));
        }

        if (jsonCompany.has(JJUConstant.JSON_KEY_BANK_ACCOUNT_OWNER)) {
            companyModel.setCompanyBankAccountOwner(jsonCompany.getString(JJUConstant.JSON_KEY_BANK_ACCOUNT_OWNER));
        }

        if (jsonCompany.has(JJUConstant.JSON_KEY_BUDGET_DATE)) {
            companyModel.setCompanyBudgetDate(jsonCompany.getInt(JJUConstant.JSON_KEY_BUDGET_DATE));
        }

        if (jsonCompany.has(JJUConstant.JSON_KEY_PROCUREMENT)) {
            JSONObject jsonObjectProcurement = jsonCompany.getJSONObject(JJUConstant.JSON_KEY_PROCUREMENT);
            jsonToProcurement(jsonObjectProcurement);
        }


        if (jsonCompany.has(JJUConstant.JSON_KEY_CONFIG)) {
            JSONObject jsonConfig = jsonCompany.getJSONObject(JJUConstant.JSON_KEY_CONFIG);
            boolean isMultipleTransaction = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_MULTIPLE_TRANSACTION);
            boolean isNeedVerify = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_NEED_VERIFY);
            boolean isHaveCashAdvance = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_HAVE_CASH_ADVANCE);

            boolean isHaveAttendance = false;
            if (jsonConfig.has(JJUConstant.JSON_KEY_IS_HAVE_ATTENDANCE)) {
                isHaveAttendance = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_HAVE_ATTENDANCE);
            }

            boolean isHaveTag = false;
            if (jsonConfig.has(JJUConstant.JSON_KEY_IS_HAVE_CASH_ADVANCE_TAG)) {
                isHaveTag = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_HAVE_CASH_ADVANCE_TAG);
            }

            boolean isHaveEmploy = false;
            if (jsonConfig.has(JJUConstant.JSON_KEY_IS_HAVE_EMPLOYEE)) {
                isHaveEmploy = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_HAVE_EMPLOYEE);
            }

            boolean isHaveServiceMonitor = false;
            if (jsonConfig.has(JJUConstant.JSON_KEY_IS_HAVE_SERVICE_MONITOR)) {
                isHaveServiceMonitor = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_HAVE_SERVICE_MONITOR);
            }

            boolean isHaveInvoice = false;
            if (jsonConfig.has(JJUConstant.JSON_KEY_IS_HAVE_INVOICE)) {
                isHaveInvoice = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_HAVE_INVOICE);
            }

            boolean isHaveSignRequest = false;
            if (jsonConfig.has(JJUConstant.JSON_KEY_IS_HAVE_SIGN_REQUEST)) {
                isHaveSignRequest = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_HAVE_SIGN_REQUEST);
            }

            boolean isNeedCheckOutDescription = false;
            if (jsonConfig.has(JJUConstant.JSON_KEY_IS_NEED_CHECKOUT_DESCRIPTION)) {
                isNeedCheckOutDescription = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_NEED_CHECKOUT_DESCRIPTION);
            }

            boolean isHaveMemberPreApproval = false;
            if (jsonConfig.has(JJUConstant.JSON_KEY_IS_HAVE_MEMBER_PRE_APPROVAL)) {
                isHaveMemberPreApproval = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_HAVE_MEMBER_PRE_APPROVAL);
            }

            boolean isHaveExtraPreApproval = false;
            if (jsonConfig.has(JJUConstant.JSON_KEY_IS_HAVE_EXTRA_PRE_APPROVAL)) {
                isHaveExtraPreApproval = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_HAVE_EXTRA_PRE_APPROVAL);
            }

            boolean isHaveExtraApprover = false;
            if (jsonConfig.has(JJUConstant.JSON_KEY_IS_HAVE_EXTRA_APPROVER)) {
                isHaveExtraApprover = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_HAVE_EXTRA_APPROVER);
            }

            boolean isHaveMember = false;
            if (jsonConfig.has(JJUConstant.JSON_KEY_IS_HAVE_MEMBER)) {
                isHaveMember = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_HAVE_MEMBER);
            }

            boolean isShowReferenceId = false;
            if (jsonConfig.has(JJUConstant.JSON_KEY_IS_SHOW_REFERENCE_ID)) {
                isShowReferenceId = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_SHOW_REFERENCE_ID);
            }

            boolean isShowUseCard = false;
            if (jsonConfig.has(JJUConstant.JSON_KEY_IS_SHOW_USE_CARD)) {
                isShowUseCard = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_SHOW_USE_CARD);
            }

            boolean isAttendanceStrictMode = false;
            if (jsonConfig.has(JJUConstant.JSON_KEY_ATTENDANCE_STRICT_MODE)) {
                isAttendanceStrictMode = jsonConfig.getBoolean(JJUConstant.JSON_KEY_ATTENDANCE_STRICT_MODE);
            }

            boolean isHaveExtraApproverBatch = false;
            if (jsonConfig.has(JJUConstant.JSON_KEY_IS_HAVE_EXTRA_APPROVER_BATCH)) {
                isHaveExtraApproverBatch = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_HAVE_EXTRA_APPROVER_BATCH);
            }

            boolean isHaveJojoCard = false;
            if (jsonConfig.has(JJUConstant.JSON_KEY_IS_HAVE_JOJOCARD)) {
                isHaveJojoCard = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_HAVE_JOJOCARD);
            }

            boolean isOnVenue = false;
            if (jsonConfig.has(JJUConstant.JSON_KEY_IS_ON_VENUE)) {
                isOnVenue = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_ON_VENUE);
            }

            boolean isCheckFace = false;
            if (jsonConfig.has(JJUConstant.JSON_KEY_IS_CHECK_FACE)) {
                isCheckFace = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_CHECK_FACE);
            }

            boolean isNeedCheckInDetail = false;
            if(jsonConfig.has(JJUConstant.JSON_KEY_IS_NEED_CHECKIN_DETAIL)){
                isNeedCheckInDetail = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_NEED_CHECKIN_DETAIL);
            }

            boolean isNeedAdditionalInfoWithin = false;
            if(jsonConfig.has(JJUConstant.JSON_KEY_IS_NEED_ADDITIONAL_INFO_WITHIN)){
                isNeedAdditionalInfoWithin = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_NEED_ADDITIONAL_INFO_WITHIN);
            }

            boolean isHaveAdditionalInfo = false;
            if(jsonConfig.has(JJUConstant.JSON_KEY_IS_HAVE_ADDITIONAL_INFO)){
                isHaveAdditionalInfo = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_HAVE_ADDITIONAL_INFO);
            }



            boolean isCashAdvanceDescriptionMandatory = false;
            if (jsonConfig.has(JJUConstant.JSON_KEY_IS_CASH_ADVANCE_DESCRIPTION_MANDATORY)) {
                isCashAdvanceDescriptionMandatory = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_CASH_ADVANCE_DESCRIPTION_MANDATORY);
            }

            boolean isReimbursementDescriptionMandatory = false;
            if (jsonConfig.has(JJUConstant.JSON_KEY_IS_REIMBURSEMENT_DESCRIPTION_MANDATORY)) {
                isReimbursementDescriptionMandatory = jsonConfig.getBoolean(JJUConstant.JSON_KEY_IS_REIMBURSEMENT_DESCRIPTION_MANDATORY);
            }


            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_MULTIPLE_TRANSACTION, isMultipleTransaction);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_NEED_VERIFY, isNeedVerify);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_HAVE_CASH_ADVANCE, isHaveCashAdvance);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_HAVE_ATTENDANCE, isHaveAttendance);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_HAVE_CASH_ADVANCE_TAG, isHaveTag);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_HAVE_EMPLOYEE, isHaveEmploy);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_HAVE_SERVICE_MONITOR, isHaveServiceMonitor);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_HAVE_INVOICE, isHaveInvoice);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_HAVE_SIGN_REQUEST, isHaveSignRequest);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_NEED_CHECKOUT_DESCRIPTION, isNeedCheckOutDescription);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_HAVE_MEMBER_PRE_APPROVAL, isHaveMemberPreApproval);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_HAVE_EXTRA_PRE_APPROVAL, isHaveExtraPreApproval);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_HAVE_EXTRA_APPROVER, isHaveExtraApprover);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_HAVE_MEMBER, isHaveMember);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_SHOW_REFERENCE_ID, isShowReferenceId);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_SHOW_USE_CARD, isShowUseCard);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_ATTENDANCE_STRICT_MODE, isAttendanceStrictMode);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_HAVE_EXTRA_APPROVER_BATCH, isHaveExtraApproverBatch);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_HAVE_JOJOCARD, isHaveJojoCard);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_ON_VENUE, isOnVenue);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_CHECK_FACE, isCheckFace);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_NEED_CHECKIN_DETAIL, isNeedCheckInDetail);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_NEED_ADDITIONAL_INFO_WITHIN, isNeedAdditionalInfoWithin);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_HAVE_ADDITIONAL_INFO, isHaveAdditionalInfo);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_CASH_ADVANCE_DESCRIPTION_MANDATORY, isCashAdvanceDescriptionMandatory);
            JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_REIMBURSEMENT_DESCRIPTION_MANDATORY, isReimbursementDescriptionMandatory);

        } else {
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_MULTIPLE_TRANSACTION);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_NEED_VERIFY);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_HAVE_CASH_ADVANCE);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_HAVE_ATTENDANCE);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_HAVE_CASH_ADVANCE_TAG);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_HAVE_EMPLOYEE);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_HAVE_SERVICE_MONITOR);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_HAVE_INVOICE);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_HAVE_SIGN_REQUEST);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_NEED_CHECKOUT_DESCRIPTION);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_HAVE_MEMBER_PRE_APPROVAL);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_HAVE_EXTRA_PRE_APPROVAL);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_HAVE_EXTRA_APPROVER);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_HAVE_MEMBER);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_SHOW_REFERENCE_ID);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_SHOW_USE_CARD);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_ATTENDANCE_STRICT_MODE);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_HAVE_EXTRA_APPROVER_BATCH);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_HAVE_JOJOCARD);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_ON_VENUE);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_CHECK_FACE);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_NEED_CHECKIN_DETAIL);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_NEED_ADDITIONAL_INFO_WITHIN);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_HAVE_ADDITIONAL_INFO);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_VENUE_STRICT);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_CASH_ADVANCE_DESCRIPTION_MANDATORY);
            JJUJojoSharePreference.removeData(JJUConstant.JSON_KEY_IS_REIMBURSEMENT_DESCRIPTION_MANDATORY);
        }

        if (jsonCompany.has(JJUConstant.JSON_KEY_SPLASH_URL)) {
            companyModel.setCompanySplashUrl(jsonCompany.getString(JJUConstant.JSON_KEY_SPLASH_URL));
        }

        return companyModel;
    }

    private void jsonToProcurement(JSONObject jsonProcurement) throws JSONException {
        JJUJojoSharePreference.putDataBoolean(JJUConstant.JSON_KEY_IS_HAVE_PROCUREMENT, true);

        if (jsonProcurement.has(JJUConstant.JSON_KEY_ROLES)) {
            for (int i = 0; i < 100; i++) {
                String key = JJUConstant.JSON_KEY_ROLES + "_" + (i + 1);
                JJUJojoSharePreference.removeData(key);
            }

            JSONArray arrayRole = jsonProcurement.getJSONArray(JJUConstant.JSON_KEY_ROLES);
            for (int i = 0; i < arrayRole.length(); i++) {
                String roleKey = JJUConstant.JSON_KEY_ROLES + "_" + arrayRole.optInt(i);
                JJUJojoSharePreference.putDataBoolean(roleKey, true);
            }
        }

        if (jsonProcurement.has(JJUConstant.JSON_KEY_FEATURES)) {
            for (int i = 0; i < 100; i++) {
                String key = JJUConstant.JSON_KEY_PROCUREMENT + "_" + (i + 1);
                JJUJojoSharePreference.removeData(key);
            }

            JSONArray arrayFeature = jsonProcurement.getJSONArray(JJUConstant.JSON_KEY_FEATURES);
            for (int i = 0; i < arrayFeature.length(); i++) {
                String featureKey = JJUConstant.JSON_KEY_PROCUREMENT + "_" + arrayFeature.optInt(i);
                JJUJojoSharePreference.putDataBoolean(featureKey, true);
            }
        }
    }



}
