package com.laluhilman.testingnetworking;

import android.app.Application;

import com.jojonomic.jojoutilitieslib.utilities.JJUBaseApplication;
import com.jojonomic.jojoutilitieslib.utilities.JJUJojoSharePreference;

/**
 * Created by laluhilman on 14/05/18.
 */

public class App extends JJUBaseApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        DatabseHelper.getSingleton(getApplicationContext());



    }

    @Override
    protected void configureUrlServer() {

    }

    @Override
    protected void startPushService() {

    }

    @Override
    protected void registerBroadcastForStartDataPusher() {

    }
}
