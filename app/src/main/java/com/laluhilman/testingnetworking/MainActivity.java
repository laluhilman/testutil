package com.laluhilman.testingnetworking;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.jojonomic.jojoutilitieslib.model.JJUUserModel;
import com.jojonomic.jojoutilitieslib.screen.activity.JJUBaseActivity;
import com.jojonomic.jojoutilitieslib.utilities.JJUJojoSharePreference;

import java.util.List;

public class MainActivity extends JJUBaseActivity implements View.OnClickListener {

    private TextView data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        data = findViewById(R.id.data);


        UserModel userModel = new UserModel();
        userModel.setId("ABC12");
        userModel.setName("PAK BOS");
        userModel.setAddress("Addres pak Bos");
        userModel.setEmail("Email Pak Bos");

        DBUserManager.getSingleton(this).insertUser(userModel);

        List<UserModel> listuser = DBUserManager.getSingleton(this).getuserModels();

//        login();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);


        Log.d("OriginValue", JJUJojoSharePreference.getDataBoolean("LOGIN")+"");

        JJUJojoSharePreference.putDataBoolean("LOGIN", false);
        Log.d("EXPECTED FALSE", JJUJojoSharePreference.getDataBoolean("LOGIN")+"");

        JJUJojoSharePreference.putDataBoolean("LOGIN", true);
        Log.d("EXPECTED TRUE", JJUJojoSharePreference.getDataBoolean("LOGIN")+"");



    }


    private void getData(){

        showLoading();

        TestConnectionManager.getSingleton().getData(new RequestListener() {
            @Override
            public void onRequestSucces(String messge, List<UserModel> listUser) {

                dismissLoading();


            }

            @Override
            public void onRequestFailed(String messge) {

                dismissLoading();

            }
        });
    }


    private void login(){
        showLoading();

        TestConnectionManager.getSingleton().requestLogin("dev1testing@mailinator.com", "12345", new LoginListener() {
            @Override
            public void onrequestSucces(String message, JJUUserModel userModel) {
                dismissLoading();
                JJUJojoSharePreference.putDataString("Email", userModel.getUserEmail());

                data.setText(userModel.getUserFirstName() + userModel.getUserLastName());

            }

            @Override
            public void onrequestFailed(String message) {
                dismissLoading();

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.fab){

            Intent toNoteActivity = new Intent(getApplicationContext(), NotesActivity.class);
            startActivityForResult(toNoteActivity, Constant.requestCode);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Constant.resultValue){
            Toast.makeText(this, "Sukses ", Toast.LENGTH_LONG).show();
        }
    }
}
