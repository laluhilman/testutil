package com.laluhilman.testingnetworking;

import java.util.List;

/**
 * Created by laluhilman on 08/05/18.
 */

public interface RequestListener {
    void onRequestSucces(String messge, List<UserModel> listUser);
    void onRequestFailed(String messge);

}
